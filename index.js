const countDown = (number) => {
    console.log(`The number you provided is ${number}`);
    for (i = number; i >= 50; i -= 5) {
      if (i === 50) {
        console.log("The current value is at 50. Terminating the loop");
        break;
      } else if (i % 10 === 0) {
        console.log("The number is divisible by 10. Skipping the number");
        continue;
      } else if (i % 5 === 0) {
        console.log(i);
      }
    }
  };
  const input = prompt("Enter A number that is divisible by 5");
  (countDown(input));
  
  // Through string
  let newWord = "";
  const removeVowel = (word) => {
    console.log(word);
    for (i = 0; i < word.length; i++) {
      if (
        word[i] === "a" ||
        word[i] === "e" ||
        word[i] === "i" ||
        word[i] === "o" ||
        word[i] === "u"
      ) {
        continue;
      } else {
        newWord += word[i];
      }
    }
    console.log(newWord);
  };
  const word = "supercalifragilisticexpialidocious";
  removeVowel(word);
  
  {
    // Through array
    let arr = [];
    const removeVowel = (word) => {
      console.log(word);
      for (i = 0; i < word.length; i++) {
        if (
          word[i] === "a" ||
          word[i] === "e" ||
          word[i] === "i" ||
          word[i] === "o" ||
          word[i] === "u"
        ) {
          continue;
        } else {
          arr.push(word[i]);
        }
      }
      console.log(arr.join(""));
    };
    const word = "supercalifragilisticexpialidocious";
    removeVowel(word);
  }
  